<?php
	$g_hostname = '{MYSQL_DB_HOST}';
	$g_db_type = 'mysqli';
	$g_database_name = '{MYSQL_DB_DATABASE}';
	$g_db_username = '{MYSQL_DB_USER}';
	$g_db_password = '{MYSQL_DB_PASSWORD}';
	$g_db_table_prefix = '';
	$g_db_table_plugin_prefix = '';
	$g_db_table_suffix = '';
	$g_default_timezone = 'America/Argentina/Buenos_Aires';
	$g_crypto_master_salt = 'B2VLK7xaZAQM3PlLn1Zs0wc+B/upzIW+e3VlkYjo0cI=';
